use alsa::seq;
use alsa::seq::{PortCap, PortType};

use std::error;
use std::thread;

pub fn spawn(blacklist: Vec<String>) {
    thread::spawn(move || {
        let mut detector = DeviceDetector::new(blacklist).unwrap();
        detector.run().unwrap();
    });
}

pub struct DeviceDetector {
    input_seq: seq::Seq,
    output_seq: seq::Seq,
    input_port: i32,
    output_port: i32,
    blacklist: Vec<String>,
    input_addrs: Vec<seq::Addr>,
    output_addrs: Vec<seq::Addr>,
}

impl DeviceDetector {
    pub fn new(blacklist: Vec<String>) -> Result<Self, Box<dyn error::Error>> {
        let input_seq = seq::Seq::open(None, Some(alsa::Direction::Capture), true)?;
        let dinfo = seq::PortInfo::empty()?;
        dinfo.set_capability(PortCap::WRITE | PortCap::SUBS_WRITE);
        dinfo.set_type(PortType::MIDI_GENERIC | PortType::APPLICATION);
        input_seq.create_port(&dinfo)?;
        let input_port = dinfo.get_port();

        let output_seq = seq::Seq::open(None, Some(alsa::Direction::Playback), true)?;
        let dinfo = seq::PortInfo::empty()?;
        dinfo.set_capability(PortCap::READ | PortCap::SUBS_READ);
        dinfo.set_type(PortType::MIDI_GENERIC | PortType::APPLICATION);
        output_seq.create_port(&dinfo)?;
        let output_port = dinfo.get_port();

        Ok(DeviceDetector {
            input_seq,
            output_seq,
            input_port,
            output_port,
            blacklist,
            input_addrs: vec![],
            output_addrs: vec![],
        })
    }

    pub fn run(&mut self) -> Result<(), Box<dyn error::Error>> {
        let probe_interval = 1000;
        let mut step = probe_interval;
        loop {
            if step >= probe_interval {
                self.probe()?;
                step = 0;
            }
            self.listen()?;
            std::thread::sleep(std::time::Duration::from_millis(1));
            step += 1;
        }
    }

    fn probe(&mut self) -> Result<(), Box<dyn error::Error>> {
        for client_info in seq::ClientIter::new(&self.input_seq) {
            for port in seq::PortIter::new(&self.input_seq, client_info.get_client()) {
                if port.get_capability().contains(PortCap::SUBS_READ)
                    && !port.get_capability().contains(PortCap::NO_EXPORT)
                {
                    let sender = seq::Addr {
                        client: port.get_client(),
                        port: port.get_port(),
                    };
                    if self.input_addrs.iter().any(|addr| *addr == sender) {
                        continue;
                    }

                    let info = &self.input_seq.get_any_client_info(sender.client);
                    let name = String::from(info.as_ref().unwrap().get_name()?);

                    if self.blacklist.iter().any(|pattern| name.contains(pattern)) {
                        continue;
                    }

                    // Try connecting to target midi device
                    println!(
                        "MIDI input detected {} ({}:{})",
                        name, sender.client, sender.port,
                    );

                    let subs = seq::PortSubscribe::empty()?;
                    subs.set_sender(sender);
                    subs.set_dest(seq::Addr {
                        client: self.input_seq.client_id()?,
                        port: self.input_port,
                    });
                    self.input_seq.subscribe_port(&subs)?;
                    self.input_addrs.push(sender);
                }

                if port.get_capability().contains(PortCap::SUBS_WRITE)
                    && !port.get_capability().contains(PortCap::NO_EXPORT)
                {
                    let receiver = seq::Addr {
                        client: port.get_client(),
                        port: port.get_port(),
                    };
                    if self.output_addrs.iter().any(|addr| *addr == receiver) {
                        continue;
                    }

                    let info = &self.output_seq.get_any_client_info(receiver.client);
                    let name = String::from(info.as_ref().unwrap().get_name()?);

                    if self.blacklist.iter().any(|pattern| name.contains(pattern)) {
                        continue;
                    }

                    // Try connecting to target midi device
                    println!("Output: {} {}:{}", name, receiver.client, receiver.port);

                    let subs = seq::PortSubscribe::empty()?;
                    subs.set_sender(seq::Addr {
                        client: self.output_seq.client_id()?,
                        port: self.output_port,
                    });
                    subs.set_dest(receiver);
                    self.output_seq.subscribe_port(&subs)?;
                    self.output_addrs.push(receiver);
                }
            }
        }

        Ok(())
    }

    pub fn listen(&mut self) -> Result<(), Box<dyn error::Error>> {
        loop {
            match self.input_seq.input().event_input_pending(true) {
                Ok(nevents) => {
                    if nevents == 0 {
                        break;
                    }
                }
                _ => break,
            }

            let input = &mut self.input_seq.input();
            let ev = input.event_input()?;
            let addr = ev.get_source();

            match ev.get_type() {
                seq::EventType::PortSubscribed => {
                    let data: seq::Connect =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    let addr = data.sender;

                    println!("Connected ({}:{})", addr.client, addr.port);
                }
                seq::EventType::PortUnsubscribed => {
                    let data: seq::Connect =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    let addr = data.sender;

                    println!("Disconnected ({}:{})", addr.client, addr.port);
                    self.input_addrs.retain(|addr_| *addr_ != addr);
                    self.output_addrs.retain(|addr_| *addr_ != addr);
                }
                seq::EventType::Noteon => {
                    let data: seq::EvNote =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    println!(
                        "[{}:{}] NOTE ON: (channel: {}, note: {}, velocity: {})",
                        addr.client, addr.port, data.channel, data.note, data.velocity,
                    );

                    self.note_on(addr, data.channel, data.note, data.velocity);
                }
                seq::EventType::Noteoff => {
                    let data: seq::EvNote =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    println!(
                        "[{}:{}] NOTE OFF: channel: {}, note: {}, velocity: {}",
                        addr.client, addr.port, data.channel, data.note, data.velocity,
                    );

                    self.note_off(addr, data.channel, data.note, data.velocity);
                }
                seq::EventType::Controller => {
                    let data: seq::EvCtrl =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    println!(
                        "[{}:{}] CC: (channel: {}, param: {}, value: {})",
                        addr.client, addr.port, data.channel, data.param, data.value,
                    );

                    self.cc(addr, data.channel, data.param, data.value);
                }
                seq::EventType::Keypress => {
                    let data: seq::EvNote =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    println!(
                        "[{}:{}] KEY PRESSURE: (channel: {}, note: {}, velocity: {})",
                        addr.client, addr.port, data.channel, data.note, data.velocity,
                    );
                }
                seq::EventType::Chanpress => {
                    let data: seq::EvCtrl =
                        ev.get_data().ok_or("Error resolving event data").unwrap();

                    println!(
                        "[{}:{}] CHANNEL PRESSURE: (channel: {}, note: {}, velocity: {})",
                        addr.client, addr.port, data.channel, data.param, data.value,
                    );
                }
                seq::EventType::Clock => {}
                seq::EventType::Sensing => {}
                t => println!("[{}:{}] Unhandled event: {:?}", addr.client, addr.port, t),
            }
        }
        Ok(())
    }

    fn note_on(&self, dest: seq::Addr, channel: u8, note: u8, velocity: u8) {
        let data = seq::EvNote {
            channel,
            note,
            velocity,
            off_velocity: 0,
            duration: 0,
        };
        let mut ev = seq::Event::new(seq::EventType::Noteon, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev).unwrap();
    }

    fn note_off(&self, dest: seq::Addr, channel: u8, note: u8, velocity: u8) {
        let data = seq::EvNote {
            channel,
            note,
            velocity,
            off_velocity: 0,
            duration: 0,
        };
        let mut ev = seq::Event::new(seq::EventType::Noteoff, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev).unwrap();
    }

    fn cc(&self, dest: seq::Addr, channel: u8, param: u32, value: i32) {
        let data = seq::EvCtrl {
            channel,
            param,
            value,
        };
        let mut ev = seq::Event::new(seq::EventType::Controller, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev).unwrap();
    }
}
