mod kernel;

fn main() {
    let blacklist = vec![
        String::from("System"),
        String::from("Midi Through"),
        String::from("System"),
        String::from("Client"),
    ];
    kernel::spawn(blacklist);
    loop {
        std::thread::sleep(std::time::Duration::from_millis(500))
    }
}
